db.users.insertOne({
	"Name":"single",
	"accomodates": "2",
	"price": "1000",
	"description": "A simple room with all the basic necessities",
	"room_available" : 10,
	"isAvailable": false
})

db.users.insertMany([
	{	
		"Name":"double",
		"accomodates": "3",
		"price": "2000",
		"description": "A room fit for a small family going on a vacation",
		"room_available" : 5,
		"isAvailable": false
	},
	{
		"Name":"queen",
		"accomodates": "4",
		"price": "4000",
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"room_available" : 15,
		"isAvailable": false
	}	
	])
db.users.find({"Name":"double"})

db.users.updateOne(
	{"Name": "queen"},
		{
			$set: {
				"room_available": 0,
			}
		}

)

db.users.deleteMany({
	"room_available": 0
})

